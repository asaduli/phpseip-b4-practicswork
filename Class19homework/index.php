<?php

include 'Cicle.php';
include 'Velocity.php';
include 'Volume.php';
include 'Avarage.php';
include 'Animal.php';


$circle = new Circle();
echo $circle->areaOfCircle(5);
echo '<br>';

$velocity = new Velocity();
echo $velocity->calculateVelocity(56, 18) . "m/s<br>";

$volume = new Volume();
echo $volume->calculateVolume(5, 10, 12);
echo '<br>';

$avrage= new Avarage();
echo $avrage ->getAvarage(10,20,30);
echo '<br>';

$animal =new Animal();
echo $animal-> diet("Dog");
