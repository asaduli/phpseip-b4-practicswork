<?php

class Calculator
{

  public $number1, $number2;

  public function __construct($num1, $num2)
  {
    $this->number1 = $num1;
    $this->number2 = $num2;
  }
  public function sum()
  {

    echo "Sum is: " . $this->number1 + $this->number2;
    echo '<br>';
  }
  public function sub()
  {

    echo "Subtrac is: " . $this->number1 - $this->number2;
    echo '<br>';
  }
  public function multi()
  {

    echo "Multiplication is: " . $this->number1 * $this->number2;
    echo '<br>';
  }
  public function div()
  {

    echo "Division  is: " . $this->number1 / $this->number2;
    echo '<br>';
  }
}

$calculator = new Calculator(20, 10);
$calculator->sum();
$calculator->sub();
$calculator->multi();
$calculator->div();
