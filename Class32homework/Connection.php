<?php
class Connection{

    public $conn;
    public function __construct()
    {
       try{
         $this->conn= new PDO("mysql:host=localhost;dbname=crud", 'root', 'root');
         $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
         
   
     }
     catch(PDOException $e){
         echo "Connection Failed";
   
         $_SESSION['msg']="Connection Failed".$e->getMessage();
        header('location:../views/index.php');
     }
    }

    public function view(){

        $query="select * from cart";
        $stmt=$this->conn->prepare($query);
        $stmt->execute();
        return $stmt->fetchAll();
    }

}