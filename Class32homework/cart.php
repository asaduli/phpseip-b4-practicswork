<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <title>Cart</title>
</head>

<body>

<div class="container-fluid">

<div class="row justify-content-center mt-5">

<div class="col-md-8 ">
<table class="table table-striped table-success table-hover  table-bordered  border-primary text-center">

<thead>
    <th>SL</th>
    <th>Product Name</th>
    <th>Unit Price</th>
    <th>Quantity</th>
    <th>Price</th>

</thead>
<tbody>

<?php 
include_once 'Connection.php';
$obj=new Connection();
$cartdata=$obj->view();
$i=1;
foreach ($cartdata as $value) {
?>

    <tr>
      <td><?=$i++?></td>
      <td><?=$value['product_name']?></td>
      <td class="unitprice"><?=$value['unit_price']?></td>
      <td><input type="number" class="qnt" value="1" min=0 onchange="updatePrice(this)"></td>
      <td> <?=$value['unit_price']?></td>
    </tr>
    <?php }?>
</tbody>
</table>
</div>


</div>

</div>
 
</body>

</html>
<script>

 function updatePrice(element){
    
    const qnt=element.value;
    const unitprice=element.parentElement.previousElementSibling.innerText;
    element.parentElement.nextElementSibling.innerText=qnt*unitprice;
   console.log(unitprice);

 

 }

</script>