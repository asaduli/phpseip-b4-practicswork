<?php
session_start(); 
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Admin LogIn</title>
    <link rel="stylesheet" href="../../assets/css/bootstrap.min.css">
</head>

<body>
    <script src="../../assets/js/bootstrap.bundle.min.js"></script>
    <div class="container-fluid ">

    <div class="row justify-content-center mt-5 ">

       <div class="col-md-8 ">

        <form action="logindata.php" method="post">
            <fieldset>
                <legend class="text-center text-primary"><h2>Admin LogIn</h2></legend>

                    <label class="text-info"><h5>UserName</h5></label>

                  <input type="text"  class="form-control" name="userName" placeholder="Enter Admin User Name"><br>
                  <label class="text-info"><h5>Password</h5></label>
                  <input  type="password" class="form-control" name="password"><br>
                  <button type="submit" class="btn btn-success btn-lg " style="width:300px; margin-left: 25%;">LogIn</button>
                  
           
            </fieldset>
        </form>

       </div>


    </div>
       

    </div>



</body>

</html>

<?php
 if(isset($_SESSION['msg'])){

    echo($_SESSION['msg']);
    unset($_SESSION['msg']);
}
