
<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="../assets/css/bootstrap.min.css">
</head>

<?php
session_start();
include_once '../src/Category.php';
$catobj = new Category();
$data = $catobj->view();

?>

<body >

    <script src="../assets/js/bootstrap.bundle.min.js"></script>

    <nav class="navbar navbar-expand-lg  navbar-dark bg-primary">
        <div class="container-fluid">
            <a class="navbar-brand" href="index.php">
                <img src="../assets/image/rupali.jpeg" alt="" width="100" height="50" class="d-inline-block align-text-top  ">
                রুপালী বাজার
            </a>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0 " style="margin-left: 25%;">
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="#">Home</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                            PRODUCTS
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="navbarDropdown">

                            <?php

                         
                            foreach ($data as  $value) {
                                # code...

                            ?>
                                <li><a class="dropdown-item" href="index.php?id=<?=$value['id']?>"><?= $value['categori_title'] ?></a></li>
                            <?php } ?>

                        </ul>
                    </li>

                </ul>

            </div>
              
           <?php  
           if(isset($_SESSION['islogin'])){ ?>
            <div class="row justify-content-end mt-2 me-1" >
            <a href="product/dashbord.php" class="  text-white text-decoration-none  btn btn-info btn-md "><strong>Admin DashBord</strong></a>
               
        </div>
           <?php 
           } else{
           ?>
            <div class="row justify-content-end mt-2 me-1" >
                <a href="admin/adminlogin.php" class="  text-white text-decoration-none  btn btn-info btn-md "><strong>LogIn</strong></a>

            </div>
            <?php }?>
        </div>
    </nav>
    <br><br><br><br>
    <br><br>
    <div class="container-fluid">
        <div class="row">
    <?php
       include_once '../src/Product.php';
       $productobj=new Product();
       $category_id=$_GET['id']?? null;
       $product=$productobj->view($category_id);
    
                            foreach ($product as  $value) {
                                # code...

                            ?>
   
   <div class="col-sm-4">
            <div class="card text-center" style="margin-top: 20px;">
            <img src="../assets/image/<?=$value['picture']?>" class="card-img-top" alt="..." height="250px">
                <div class="card-body">
                    <h5 class="card-title"><?=$value['product_name']?></h5>
                    <p class="card-text"><?=$value['description']?></p>
                    <a href="product/details.php?id=<?=$value['id']?>" class="btn btn-primary">Details</a>
                </div>
            </div>
        </div>
      
        <?php }?>
    </div>
    </div>
</body>

</html>
<?php

if(isset($_SESSION['msg'])){
    echo($_SESSION['msg']);
    unset($_SESSION['msg']);
}

