<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../../assets/css/bootstrap.min.css">
    <title>Add New Product</title>
</head>
<?php
include_once '../../src/Category.php';
$catobj = new Category();
$data = $catobj->view();
?>

<body>
    <script src="../../assets//js/bootstrap.bundle.min.js"></script>
    <nav class="navbar navbar-expand-lg nnavbar navbar-dark bg-dark">
        <div class="container-fluid">
            <a class="navbar-brand" href="../index.php">
                <img src="../../assets/image/rupali.jpeg" alt="" width="100" height="60" class="d-inline-block align-text-top">
                রুপালী বাজার
            </a>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0 " style="margin-left: 25%;">
                    <li class="nav-item">
                        <a class="nav-link " aria-current="page" href="index.php">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" href="#">Product</a>

                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="../Category/add.php">Category</a>
                        
                    </li>

                </ul>

            </div>
        </div>
    </nav>

    <div class="container-fluid">


        <form class="row g-3 " style="margin-top: 50px;" action="create.php" method="POST" enctype="multipart/form-data">
            <div class="col-md-6 text-center">
                <a href="productlist.php">Product List</a>
            </div>


            <div class="col-6">
                <div class="col-md-6 justify-content-center">

                    <label for="name" class="form-label">Product Name</label>
                    <input type="text" class="form-control" id="name" name="pname">

                </div>
                <div class="col-md-6">
                    <label for="price" class="form-label">Prduct Price</label>
                    <input type="number"  min=0 max=10 class="form-control" id="price" placeholder="Enter Product Price" name="price">
                </div>
                <div class="col-md-6">
                    <div class="col-md-6">
                        <label for="floatingTextarea2">Product Descripton</label>
                        <textarea class="form-control" placeholder="Leave a comment here" id="floatingTextarea2" style="height: 100px;width: 350px;" name="description">
                        </textarea>

                    </div>
                </div>


                <div class="col-md-6">
                    <label for="inputState" class="form-label">Category </label>
                    <select id="inputState" class="form-select" name="category_id">
                        <option selected>Choose A Product Category</option>
                        <?php
                        foreach ($data as $value) {
                            # code...

                        ?>
                            <option value="<?= $value['id'] ?>"><?= $value['categori_title'] ?></option>
                        <?php } ?>
                    </select>
                </div><br>
                <div class="col">
                    <label for="picture">Product Photo</label><br>
                    <input type="file" name="picture" id="picture">

                </div><br>
                <div class="col">
                    <button type="submit" class="btn btn-primary ">ADD</button>
                </div>
            </div>


        </form>

    </div>


</body>

</html>