<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Admin Dashbord</title>
  <link rel="stylesheet" href="../../assets/css/sidebarstyle.css">
  <link rel="stylesheet" href="../../assets/css/bootstrap.min.css">
</head>

<body>
  <script src="../../assets/js/bootstrap.bundle.min.js"></script>
  <?php
  session_start();
  if (isset($_SESSION['islogin'])) {
  ?>
    <!-- The sidebar -->
    <div class="sidebar">
      <a href="../index.php">
        <img src="../../assets/image/rupali.jpeg" alt="" height="40px" width="40px" style="margin-left: 0px;">
        রুপালী বাজার
      </a>




      <a class="active" href="#home">Home</a>
      <a href="productlist.php">Products</a>
      <a href="../Category/categorylist.php">Category</a>
      <a href="trash.php">Trash List</a>
    </div>

    <!-- Page content -->
    <div class="content ">

      <div class="row justify-content-end mt-2">
        <a href="../admin/logout.php" class=" col-md-1 text-center btn btn-success btn-sm">
          <h6>Logout</h6>
        </a>
      </div>
      <h1 class="text-center text-info">Wellcome to Dashbord</h1>
    </div>


  <?php
  } else {
  ?>
    <div class="content">
      <h4 class="text-info pt-4 text-center">You are not Log In ..Please <a href="../admin/adminlogin.php">LogIn</a></h4>
    </div>

  <?php
  }
  ?>
</body>

</html>