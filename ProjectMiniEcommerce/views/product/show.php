<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="../../assets/css/bootstrap.min.css">
    <style>
        .button {
            background-color: #4CAF50;
            /* Green */
            border: none;
            color: white;
            padding: 15px 32px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 16px;
            margin: 4px 2px;
            position: relative;
            bottom: 0px;
            cursor: pointer;
            width: 100%;


        }
    </style>
</head>

<body>
    <script src="../../assets//js/bootstrap.bundle.min.js"></script>
    <nav class="navbar navbar-expand-lg nnavbar navbar-dark bg-dark">
        <div class="container-fluid">
            <a class="navbar-brand" href="../index.php">
                <img src="../../assets/image/rupali.jpeg" alt="" width="100" height="60" class="d-inline-block align-text-top">
                রুপালী বাজার
            </a>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0 " style="margin-left: 25%;">
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="#">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Link</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                            PRODUCTS
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="navbarDropdown">

                            <?php
                            foreach ($data as  $value) {
                                # code...

                            ?>
                                <li><a class="dropdown-item" href="#"><?= $value['categori_title'] ?></a></li>
                            <?php } ?>

                        </ul>
                    </li>

                </ul>

            </div>
        </div>
    </nav>

    <?php
    include_once '../../src/Product.php';
    $productobj = new Product();
    $productdata = $productobj->show($_GET['id']);
    ?>
    <div class="container-fluid">
        <a href="productlist.php">Back</a>
        <div class="row ">

            <div class="col-md-6 text-center" style="margin-top: 100px;">
                <img src="../../assets/image/<?= $productdata['picture'] ?>" alt="Product Pic" height="400px">
            </div>
            <div class="col-md-6 text-center" style="margin-top: 100px;">

                <h3>Product Name: <i><?= $productdata['product_name'] ?></i> </h3>
                <h3>Price: <i><?= $productdata['price'] ?> (Taka)</i> </h3>
                <h5>Description: </h5>
                <p><?= $productdata['description'] ?></p>
                <a href="edit.php?id=<?=$productdata['id']?>"><button class="button">EDIT</button><br>
                </a>

            </div>


        </div>
    </div>

</body>

</html>