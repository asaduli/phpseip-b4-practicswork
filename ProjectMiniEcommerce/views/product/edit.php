<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../../assets/css/bootstrap.min.css">
    <title>Add New Product</title>
</head>
<?php
include_once '../../src/Product.php';
$pobj = new Product();
$data = $pobj->show($_GET['id']);
?>
<body>
    <script src="../../assets//js/bootstrap.bundle.min.js"></script>
    <nav class="navbar navbar-expand-lg nnavbar navbar-dark bg-dark">
        <div class="container-fluid">
            <a class="navbar-brand" href="../index.php">
                <img src="../../assets/image/rupali.jpeg" alt="" width="100" height="60" class="d-inline-block align-text-top">
                রুপালী বাজার
            </a>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0 " style="margin-left: 25%;">
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="#">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#"></a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                            PRODUCTS
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="navbarDropdown">




                            <li><a class="dropdown-item" href="#"></a></li>


                        </ul>
                    </li>

                </ul>

            </div>
        </div>
    </nav>

    <div class="container-fluid">

        <a href="index.php">Back</a>

        <form class="row g-3 " style="margin-top: 50px;" action="update.php" method="POST" enctype="multipart/form-data">
            <div class="col-6"></div>


            <div class="col-6">
                <div class="col-md-6">

                <input type="hidden" name="id" value="<?=$data['id']?>">

                    <label for="name" class="form-label">Product Name</label>
                    <input type="text" class="form-control" id="name" name="pname" value="<?= $data['product_name'] ?>">

                </div>
                <div class="col-md-6">
                    <label for="price" class="form-label">Prduct Price</label>
                    <input type="number" class="form-control" id="price" value="<?= $data['price'] ?>" name="price">
                </div>
                <div class="col-md-6">
                    <div class="col-md-6">
                        <label for="floatingTextarea2">Product Descripton</label>
                        <textarea class="form-control text-left" id="floatingTextarea2" style="height: 100px;width: 350px;" name="description">
                    <?= $data['description'] ?>
                        </textarea>

                    </div>
                </div>

                <?php
                include_once '../../src/Category.php';
                $catobj = new Category();
                $category = $catobj->view();
                $catselected=$catobj->categorById($data['categori_id']);
                ?>
                <div class="col-md-6">
                    <label for="inputState" class="form-label">Category </label>
                    <select id="inputState" class="form-select" name="category_id">
                        <!-- <option selected><?=$catselected['categori_title']?></option> -->
                        <?php
                        foreach ($category as $value) {
                            # code...

                        ?>
                            <option value="<?= $value['id'] ?>" <?php if(in_array($catselected['categori_title'],$value)){  ?> selected <?php }?>><?= $value['categori_title'] ?></option>
                        <?php } ?>
                    </select>
                </div><br>
                <br>
                <div class="col">
                    <button type="submit" class="btn btn-primary " >Update</button>
                </div>
            </div>


        </form>

    </div>


</body>

</html>
<?php 

