<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../../assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../assets/css/sidebarstyle.css">
    <title>Trash List</title>

</head>

<body>
    <script src="../../assets//js/bootstrap.bundle.min.js"></script>
    <?php
    include_once '../../src/Product.php';
    $productobj = new Product();
    $product = $productobj->trash();
    if (isset($_SESSION['islogin'])) {

    ?>
        <div class="sidebar">
            <a href="../index.php">
                <img src="../../assets/image/rupali.jpeg" alt="" height="40px" width="40px" style="margin-left: 0px;">
                রুপালী বাজার</a>
            <a href="dashbord.php">Home</a>
            <a href="productlist.php">Products</a>
            <a href="../Category/categorylist.php">Category</a>
            <a href="#about" class="active">Trash List</a>
        </div>

        <div class="container-fluid">
            <div class="row justify-content-end mt-2">
                <a href="../admin/logout.php" class=" col-md-1 text-center btn btn-success btn-sm">
                    <h6>Logout</h6>
                </a>
            </div>

            <div class="row justify-content-md-center">

                <div class="col-md-6 mt-5 text-center">
                    <?php
                    if (isset($_SESSION['msg'])) {
                        echo ($_SESSION['msg']);
                        unset($_SESSION['msg']);
                    }
                    ?>

                    <h4 class="text-info">Trash List: </h4>
                    <table class=" table  table-success table-striped table-hover  table-bordered border-info">
                        <thead>
                            <tr>
                                <th scope="col">SL</th>
                                <th scope="col">Product Name</th>
                                <th scope="col">Category ID</th>
                                <th scope="col">Price</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $i = 1;

                            foreach ($product as  $value) {

                            ?>
                                <tr>
                                    <th scope="row"><?= $i++ ?></th>
                                    <td><?= $value['product_name'] ?></td>
                                    <td><?= $value['categori_id'] ?></td>
                                    <td><?= $value['price'] ?></td>
                                    <td>
                                        <a href="restore.php?id=<?= $value['id'] ?>" class="btn btn-info btn-sm ">Restore</a>
                                        <a href="parmanant_delete.php?id= <?= $value['id'] ?>" onclick="return confirm('Are you sure to delete this item?')" class="btn btn-info btn-sm ">Parmanent DELETE</a>
                                    </td>
                                </tr>
                            <?php
                            } ?>
                        </tbody>
                    </table>
                </div>

            </div>


        </div>
    <?php
    } else {
        ?>
            <div class="content">
    
                <p>
                <h4 class="text-info pt-4 text-center">You are not Log In ..Please <a href="../admin/adminlogin.php">LogIn</a>
                </h4>
            </div>
    
        <?php
        }
        ?>



</body>

</html>