<?php

include_once 'Connection.php';
class Product extends Connection{

    
    public function store($data){
     
        $name=$data['pname'];
        $description=$data['description'];
        $price=$data['price'];
        $category_id=$data['category_id'];
        $filename=$_FILES['picture']['name'];
        $tempname=$_FILES['picture']['tmp_name'];
        $uniqueimagename=time().$filename;
        move_uploaded_file($tempname,'../../assets/image/'.$uniqueimagename);
        try{
          $query="insert into products(product_name,description,price,categori_id,picture) values('$name','$description','$price','$category_id','$uniqueimagename')";
          $this->conn->exec($query);
        
          $_SESSION['msg']="Data Added Successfully!";
        }catch(PDOException $ex){
            $_SESSION['msg']= "Data Added Failed".$ex->getMessage();
        }
        
      }
      public function view($id=null){
        
         if($id){
         $query="select * from products where categori_id=$id and  is_deleted is null";
         }
         else{

            $query="select * from products where is_deleted is null";

         }
         
          $stmt=$this->conn->prepare($query);
          $stmt->execute();
          return $stmt->fetchAll();
         
      }
      public function show($id){
        
    
        $query="select * from products where id=$id ";
        
        
         $stmt=$this->conn->prepare($query);
         $stmt->execute();
         return $stmt->fetch();
        
     }
     public function delete($id){
       
      $query="delete from products where id=$id";
      $stmt=$this->conn->prepare($query);
      $stmt->execute();
      $_SESSION['msg']="Successfully Deleted Parmanently";

     }
     public function update($data){
      $name=$data['pname'];
      $description=$data['description'];
      $price=$data['price'];
      $category_id=$data['category_id'];
      $id=$data['id'];
       try {
         $query="update products set product_name=:p_name, description=:des,price=:price,categori_id=:cat_id where id=$id";
         $stmt=$this->conn->prepare($query);
         $stmt->execute(
           [
             'p_name'=>$name,
             'des'=>$description,
             'price'=>$price,
             'cat_id'=>$category_id,
           ]
         );
         $_SESSION['msg']="Data Updated Successfully";
       } catch (PDOException $ex) {
          $_SESSION['msg']="Data Update Failed".$ex->getMessage();
       }


     }
     public function destroy($id){
      try{
        $query="update products set is_deleted=:delete where id=:id";
        $stmt=$this->conn->prepare($query);
        $stmt->execute(
          [
            'delete'=>true,
            'id'=>$id
          ]
         
        );
       $_SESSION['msg']="Successfully Deleted";
      }catch(PDOException $ex){
        $_SESSION['msg']="Failed to Delete".$ex->getMessage();

      }
     

     }
     public function trash(){
       
      try {
      $query="select * from products where is_deleted= 1";
      $stmt=$this->conn->prepare($query);
      $stmt->execute();
      return $stmt->fetchAll();
      } catch (PDOException $ex) {
         $_SESSION['error']=$ex->getMessage();
      }

     }
     public function restore($id){
      try{
        $query="update products set is_deleted=:delete where id=:id";
        $stmt=$this->conn->prepare($query);
        $stmt->execute(
          [
            'delete'=>null,
            'id'=>$id
          ]
        
        );
        $_SESSION['msg']="Successfully Restored Data";
       
      }catch(PDOException $ex){
        $_SESSION['error']=$ex->getMessage();

      }
     

     }
     
      
}