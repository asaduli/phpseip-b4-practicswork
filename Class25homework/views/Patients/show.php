<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Show Patients Details</title>
</head>
<body>
    <a href="../index.php">Back To Home</a>
    <?php 
     include_once '../../src/Patients.php';
     $patientobj=new Patients();
     $data=$patientobj->show($_GET['id']);
    ?>
   
   <h1>Patients Details:</h1>
   <br>
   
   <h3>First Name: <?=$data['fname']?></h3>
   <h3>Last Name: <?=$data['lname']?></h3>
   <h3>Email: <?=$data['email']?></h3>
   <h3>Phone: <?=$data['phone']?></h3>
    
</body>
</html>
<?php 
