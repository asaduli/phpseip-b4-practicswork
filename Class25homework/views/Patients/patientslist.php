<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Patients List</title>
</head>
 <?php 
  include_once'../../src/Patients.php';
  $petientsob=new Patients();
  $data=$petientsob->view();   
 ?>
<body>
    <a href="../index.php">Back to Home</a>
    <br>
    <a href="addPateints.php">ADD NEW</a>
    <h1>Patienst List</h1>

     <table border="1px" align="center">
      <thead>
          <tr>
            <th>SL</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Email</th>
            <th>Action</th>
          </tr>
      </thead>
      <tbody>
          <?php 
          $i=0;
           foreach ($data as  $value) {
               # code...
           
          ?>
          <tr>
              <td><?= ++$i?></td>
              <td><?= $value['fname']?></td> 
              <td><?= $value['lname']?></td> 
              <td><?= $value['email']?></td> 
              <td>
                  <a href="show.php?id=<?=$value['id']?>">SHOW||</a>
                  <a href="edit.php?id=<?=$value['id']?>">EDIT||</a>
                  <a href="delete.php?id=<?=$value['id']?>">DELETE</a>
              </td>
          </tr>
          <?php }?>
      </tbody>

     </table>
</body>
</html>