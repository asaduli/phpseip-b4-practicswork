<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Bill Page</title>
</head>

<body>
    <a href="index.php">Back to Home</a>
    <h1>Add Bill</h1>
    <form action="../formdata/billdata.php" method="post">
    <fieldset>
        <legend>Add New Bill</legend>
        <input type="number" name="amount" placeholder="Enter Bill Amount"><br>
        <input type="number" name="doctor_id" placeholder="Enter Doctor ID"><br>
        <button type="submit">Add Bill</button>
    </fieldset>
     
    </form>
</body>

</html>