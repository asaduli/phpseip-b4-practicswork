<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Bill List</title>
</head>
 <?php 
  include_once'../../src/Bill.php';
  $billobj=new Bill();
  $data=$billobj->view();   
 ?>
<body>
    <a href="../index.php">Back to Home</a>
    <h1>Bill List</h1>

     <table border="1px">
      <thead>
          <tr>
            <th>SL</th>
            <th>Bill ID</th>
            <th>Amount</th>
            <th>Doctor ID</th>
        
          </tr>
      </thead>
      <tbody>
          <?php
           foreach ($data as  $value) {
            
           
          ?>
          <tr>
              <td>1</td>
              <td><?= $value['id']?></td> 
              <td><?= $value['amount']?></td> 
              <td><?= $value['doctor_id']?></td> 
             
          </tr>
          <?php }?>
      </tbody>

     </table>
</body>
</html>