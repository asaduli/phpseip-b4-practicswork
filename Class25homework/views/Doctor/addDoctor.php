<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Add Doctor</title>
</head>
<body>
    <a href="../index.php">Back to Home</a>
    <h1>Add New Doctor</h1>

    <form action="doctordata.php" method="POST">
     <fieldset>
     <legend>
         Add New Dcotor Form
     </legend>

      <input type="text" name="name" placeholder="Enter Doctor Name"><br>
      <input type="text" name="degree" placeholder="Enter Doctor Degree"><br>
      <input type="text" name="specialist" placeholder="Enter Doctor Specialist Area"><br>
      <input type="text" name="phone" placeholder="Enter Phone Number"><br>
      <button type="submit">Add Doctor</button>
     </fieldset>

    </form>
    
</body>
</html>
<?php
session_start();
if(isset($_SESSION['msg'])){
    echo($_SESSION['msg']);
    unset($_SESSION['msg']);
}