<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Patients List</title>
</head>
 <?php 
  include_once '../../src/Doctors.php';
  $doctorob=new Doctors();
  $data=$doctorob->view();   
 ?>
<body>
    <a href="../index.php">Back to Home</a>
    <br><br>
    <a href="addDoctor.php">ADD NEW DOCTOR</a>
    <h1>Doctor  List</h1>

     <table border="1px">
      <thead>
          <tr>
            <th>SL</th>
            <th>Dr Name</th>
            <th>Degree</th>
            <th>Specialist</th>
            <th>Phone</th>
            <th>Action</th>
          </tr>
      </thead>
      <tbody>
          <?php
           foreach ($data as  $value) {
            
           
          ?>
          <tr>
              <td>1</td>
              <td><?= $value['name']?></td> 
              <td><?= $value['degree']?></td> 
              <td><?= $value['specialist']?></td> 
              <td><?= $value['phone']?></td> 
              <td>
                  <a href="show.php?id=<?= $value['id']?>">SHOW||</a>
                  <a href="edit.php?id=<?= $value['id']?>">EDIT||</a>
                  <a href="delete.php?id=<?= $value['id']?>">DELETE</a>
              </td>
          </tr>
          <?php }?>
      </tbody>

     </table>
</body>
</html>