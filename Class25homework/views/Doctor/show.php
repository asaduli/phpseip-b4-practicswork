<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Show Patients Details</title>
</head>
<body>
    <a href="../index.php">Back To Home</a>
    <?php 
     include_once '../../src/Doctors.php';
     $doctorobj=new Doctors();
     $data=$doctorobj->show($_GET['id']);
    ?>
   
   <h1>Doctor Details:</h1>
   <br>
   
   <h3>Doctor Name: <?=$data['name']?></h3>
   <h3>Degree: <?=$data['degree']?></h3>
   <h3>Specialis: <?=$data['specialist']?></h3>
   <h3>Phone: <?=$data['phone']?></h3>
    
</body>
</html>
<?php 
