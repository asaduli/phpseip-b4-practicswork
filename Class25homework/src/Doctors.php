<?php
include_once 'Connection.php';

class Doctors extends Connection
{


    public function store($data)
    {


        $name = $data['name'];
         
        $degree = $data['degree'];
        $specialist = $data['specialist'];
        $phone = $data['phone'];


        try {

            $query = "insert into doctors (name,degree,specialist,phone) values (:name,:degree,:specialist,:phone)";
            $stmt = $this->conn->prepare($query);
            $stmt->execute([
                'name' => $name,
                'degree' => $degree,
                'specialist' => $specialist,
                'phone' => $phone,

            ]);
            echo "Data Inserted";
        } catch (PDOException $ex) {
            $_SESSION['msg'] = "Data Submision Failed" . $ex;
            header('location:../views/addDoctor.php');
        }
    }
    public function view()
    {

        try {
            $query = "select * from doctors";
            $stmt = $this->conn->prepare($query);
            $stmt->execute();
            $data = $stmt->fetchAll();
            return $data;
        } catch (PDOException $er) {

            $_SESSION['error'] = $er;
        }
    }
    public function show($id)
    {

        try {
            $query = "select * from doctors where id = :id";
            $stmt = $this->conn->prepare($query);
            $stmt->execute([
                'id' => $id
            ]);
            $data = $stmt->fetch();
            return $data;
        } catch (PDOException $ex) {
            $_SESSION['error'] = $ex->getMessage();
        }
    }
    public function delete($id)
    {

        try {
            $query = "delete from doctors where id= :p_id";
            $stmt = $this->conn->prepare($query);
            $stmt->execute(
                [
                    'p_id' => $id
                ]
            );
        } catch (PDOException $ex) {
            $_SESSION['error'] = $ex->getMessage();
        }
    }
    public function edit($id)
    {

        try {
            $query = "select * from doctors where id=:d_id";
            $stmt = $this->conn->prepare($query);
            $stmt->execute(
                [
                    'd_id' => $id
                ]
            );
            return $data = $stmt->fetch();
        } catch (\Throwable $th) {
        }
    }
    public function update($data){

        try{
         $ID=$data['id'];
         $name=$data['name'];
         $degree=$data['degree'];
         $specialist=$data['specialist'];
         $phone=$data['phone'];

         $query="update doctors set name=:name,degree=:degree,specialist=:specialist,phone=:phone where id=:id";
         $stmt=$this->conn->prepare($query);
         $stmt->execute(
             [
                 'name'=>$name,
                 'degree'=>$degree,
                 'specialist'=>$specialist,
                 'phone'=>$phone,
                 'id'=>$ID
             ]
         );
        
        }catch(PDOException $ex){
        $_SESSION['error']=$ex->getMessage();
        }

        
    }
}
