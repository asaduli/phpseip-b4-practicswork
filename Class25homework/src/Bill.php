<?php
include_once 'Connection.php';
class Bill extends Connection{

    public function store($data)
    {
        
    $amount=$data['amount'];
    $doctor_id=$data['doctor_id'];

    

    try {
    
        $query="insert into bills (amount,doctor_id) values (:amount,:doctor_id)";
        $stmt=$this->conn->prepare($query);
        $stmt->execute([
            'amount'=>$amount,
            'doctor_id'=>$doctor_id,
        ]);
        echo "Data Inserted";
    }
    catch(PDOException $ex){
        $_SESSION['msg']= "Data Submision Failed".$ex;
       header('location:../views/addBill.php');
    }
    }

    public function view(){

        try {
          $query="select * from bills";
          $stmt=$this->conn->prepare($query);
          $stmt->execute();
          $data=$stmt->fetchAll();
          return $data;
        } catch (PDOException $er) {
            
          $_SESSION['error']=$er;
          
        }
    }


}