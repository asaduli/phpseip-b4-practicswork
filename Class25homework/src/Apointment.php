<?php
 include_once 'Connection.php';
class Apointment extends Connection{

    public function store($data)
    {
      
        try {

            $pname = $data['p_name'];
            $doctor_id=$data['doctor_id'];
        
            $query = "insert into apointments (pname,doctor_id) values (:pname,:doctor_id)";
            $stmt = $this->conn->prepare($query);
            $stmt->execute([
                'pname' => $pname,
               'doctor_id'=>$doctor_id,

            ]);
            echo "Data Inserted";
        } catch (PDOException $ex) {
            $_SESSION['msg'] = "Data Submision Failed" . $ex->getMessage();
            header('location:../views/addApoinment.php');
        }
    }
    public function view()
    {

        try {
            $query = "select * from apointments";
            $stmt = $this->conn->prepare($query);
            $stmt->execute();
            $data = $stmt->fetchAll();
            return $data;
        } catch (PDOException $er) {

            $_SESSION['error'] = $er;
        }
    }
    

}
