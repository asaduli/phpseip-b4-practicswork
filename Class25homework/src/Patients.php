<?php
include_once 'Connection.php';
class Patients extends Connection
{


    public function printData($data)
    {
        print_r($data);
    }

    public function store($data)
    {

        $fname = $data['fname'];
        $lname = $data['lname'];
        $email = $data['email'];
        $password = $data['password'];
        $phone = $data['phone'];


        try {

            $query = "insert into patients (fname,lname,email,password,phone) values (:fast_name,:last_name,:email,:password,:phone)";
            $stmt = $this->conn->prepare($query);
            $stmt->execute([
                'fast_name' => $fname,
                'last_name' => $lname,
                'email' => $email,
                'password' => $password,
                'phone' => $phone,

            ]);
        } catch (PDOException $ex) {
            $_SESSION['msg'] = "Data Submision Failed" . $ex;
            header('location:../views/Patients/addPateints.php');
        }
    }

    public function view()
    {

        try {
            $query = "select * from patients";
            $stmt = $this->conn->prepare($query);
            $stmt->execute();
            $data = $stmt->fetchAll();
            return $data;
        } catch (PDOException $er) {

            $_SESSION['error'] = $er->getMessage();
        }
    }
    public function show($id)
    {

        try {
            $query = "select * from patients where id = :id";
            $stmt = $this->conn->prepare($query);
            $stmt->execute([
                'id' => $id
            ]);
            $data = $stmt->fetch();
            return $data;
        } catch (PDOException $ex) {
            $_SESSION['error'] = $ex->getMessage();
        }
    }
    public function delete($id)
    {

        try {
            $query = "delete from patients where id= :p_id";
            $stmt = $this->conn->prepare($query);
            $stmt->execute(
                [
                    'p_id' => $id
                ]
            );
        } catch (PDOException $ex) {
            $_SESSION['error'] = $ex->getMessage();
        }
    }
    public function edit($id)
    {

        try {
            $query = "select * from patients where id=:p_id";
            $stmt = $this->conn->prepare($query);
            $stmt->execute(
                [
                    'p_id' => $id
                ]
            );
            return $data = $stmt->fetch();
        } catch (\Throwable $th) {
        }
    }
    public function update($data){

        try{
         $ID=$data['id'];
         $fname=$data['fname'];
         $lname=$data['lname'];
         $email=$data['email'];
         $phone=$data['phone'];

         $query="update patients set fname=:fast_name,lname=:last_name,email=:email,phone=:phone where id=:id";
         $stmt=$this->conn->prepare($query);
         $stmt->execute(
             [
                 'fast_name'=>$fname,
                 'last_name'=>$lname,
                 'email'=>$email,
                 'phone'=>$phone,
                 'id'=>$ID
             ]
         );
        
        }catch(PDOException $ex){
        $_SESSION['error']=$ex->getMessage();
        }

        
    }
}
