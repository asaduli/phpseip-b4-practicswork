<?php 

class Connection{

public $conn;

  public function __construct()
  {

    try{
        $this->conn= new PDO("mysql:host=localhost;dbname=asadul", 'root', 'root');
        $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        echo "connection success";

    }
    catch(PDOException $e){
        echo "Connection Failed";
        session_start();
        $_SESSION['msg']="Connection Failed".$e->getMessage();
       header('location:../view/index.php');
    }
    
  }

  public function store($data){

    $fname=$data['fname'];
    $lname=$data['lname'];
    

    try {
    
        $query="insert into users (fname,lname) values (:fast_name,:last_name)";
        $stmt=$this->conn->prepare($query);
        $stmt->execute([
            'fast_name'=>$fname,
            'last_name'=>$lname,

        ]);
        echo "Data Inserted";
    }
    catch(PDOException $ex){
        session_start();
        $_SESSION['msg']= "Data Submision Failed";
       header('location:../view/index.php');
    }

    } 
 }