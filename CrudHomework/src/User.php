<?php
class User
{

    public $conn;
    public function __construct()
    {
        session_start();

        try {
            $this->conn = new PDO("mysql:host=localhost;dbname=crud", 'root', 'root');
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $ex) {
            $_SESSION['message'] = $ex->getMessage();
        }
    }
    public function store($data){
      try{
       $name=$data['name'];
       $father_name=$data['fname'];
       $mother_name=$data['mname'];
       $phone=$data['phone'];
       $email=$data['email'];
       $date=$data['date'];
       $gender=$data['gender'];
       $address=$data['address'];
       $blood_group=$data['blood'];
       $department=$data['department'];
       $course=implode(",",$data['course']);
      
       $query="insert into users (name,father_name,mother_name,phone,email,date,gender,address,blood_group,department,course) 
                 values(:name,:fname,:mname,:phone,:email,:date,:gender,:address,:blood,:department,:course)";
        $stmt=$this->conn->prepare($query);
        $stmt->execute(
            [
                'name'=>$name,
                'fname'=>$father_name,
                'mname'=>$mother_name,
                'phone'=>$phone,
                'email'=>$email,
                'date'=>$date,
                'gender'=>$gender,
                'address'=>$address,
                'blood'=>$blood_group,
                'department'=>$department,
                'course'=>$course,
            ]
        );  
      

      }catch(PDOException $ex){
          $_SESSION['message']=$ex->getMessage();
      }

    }
    public function view(){

        $query="select * from users";
        $stmt=$this->conn->prepare($query);
        $stmt->execute();

        return $stmt->fetchAll();
    }

    public function show($id){
        $query="select * from users where id=:id";
        $stmt=$this->conn->prepare($query);
        $stmt->execute(
            [
                'id'=>$id
            ]
        );
        return $stmt->fetch();
    }
    public function delete($id){

        $query="delete from users where id=:id";
        $stmt=$this->conn->prepare($query);
        $stmt->execute([
            'id'=>$id
        ]);
    }

 
}
