<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>User Info</title>
    <style>

    
    </style>

    <?php
    include_once '../src/User.php';
    $userobj = new User();
    $data = $userobj->show($_GET['id']);
    ?>
</head>

<body>
    <a href="userlist.php">BACK</a>
    <h1>User Details</h1>
    <br>
    <div style="text-align: center;">
        <h3>Name: <?= $data['name'] ?></h3>
        <h3>Father Name: <?= $data['father_name'] ?></h3>
        <h3>Mother Name: <?= $data['mother_name'] ?></h3>
        <h3>Email: <?= $data['email'] ?></h3>
        <h3>Phone: <?= $data['phone'] ?></h3>
        <h3>Gender: <?= $data['gender'] ?></h3>
        <h3>Address: <?= $data['address'] ?></h3>
        <h3>Blood Group: <?= $data['blood_group'] ?></h3>
        <h3>Department: <?= $data['department'] ?></h3>
        <h3>Course: <?= $data['course'] ?></h3>
    </div>
</body>

</html>