<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>User List</title>
</head>
<body>
    <?php 
      include_once '../src/User.php';
      $userobj=new User();
      $userdata=$userobj->view();
    ?>
    <a href="index.html">Add NEW</a>


     <table border="1px" align="center" >
       <thead>

       <tr>
           <th>SL</th>
           <th>Name</th>
           <th>Email</th>
           <th>Phone</th>
           <th>Department</th>
           <th>Course</th>
           <th>Action</th>
       </tr>

       </thead>
       <tbody>

         <?php
         $i=1;
          foreach ($userdata as  $value) {
              # code...
          
         
         ?>
           <tr>
               
             <td><?=$i++?></td>
             <td><?=$value['name']?></td>
             <td><?=$value['email']?></td>
             <td><?=$value['phone']?></td>
             <td><?=$value['department']?></td>
             <td><?=$value['course']?></td>
             <td>
                 <a href="show.php?id=<?=$value['id']?>">SHOW</a>
                 <a href="edit.php?id=<?=$value['id']?>">EDIT</a>
                 <a href="delete.php?id=<?=$value['id']?>">DELETE</a>
             </td>
             
           </tr>
           <?php
          }
           ?>


       </tbody>

     </table>
    
</body>
</html>